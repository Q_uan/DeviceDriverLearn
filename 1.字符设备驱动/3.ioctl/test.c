#include<stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "myioctl.h"

#define NAME "/dev/demo"
 int main(int argc, const char *argv[])
{
	int ret;
	int i=0;
	int fd=open(NAME,O_RDWR);
	if(fd<0)
	{
		perror("open");//void perror(const char *s);  perror——输出用户信息及errno对应的错误信息
	}
	ret=ioctl(fd,ONE);
	if(0!=ret)
	{
		perror("ioctl");
	}
	ret=ioctl(fd,TWO);
	if(0!=ret)
	{
		perror("ioctl");
	}
	close(fd);
	return 0;
}
/***********************知识总结*************************
#include <sys/ioctl.h>
int ioctl(int d, int request, ...);
返回值：成功 0 
失 败 ： -1 并设置错误码 
**********************************************************/
