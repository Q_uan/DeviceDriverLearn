#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/device.h>
#include "myioctl.h"

#define NAME "demo"
int major;
#define CLASS_NAME "democlass"
struct class * class;
struct device * device;
int demo_open(struct inode *inode , struct file *file)
{
	printk("demo_open\n");
	return 0;
}
int demo_release  (struct inode *inode, struct file *file)
{
	printk("demo_release\n");
	return 0;
}
long demo_ioctl (struct file *file, unsigned int cmd , unsigned long arg)
{
	switch(cmd)
	{
		case ONE:
			printk("command one\n");
			break;
		case TWO:
			printk("command TWO\n");
			break;
		case THREE:
			printk("command THREE\n");
			break;
		default:
			printk("command default\n");
			break;	
	}
	return 0;
}

const struct file_operations fops={
	.owner=THIS_MODULE,
	.open=demo_open,
	.release=demo_release,
	.unlocked_ioctl=demo_ioctl,
};
static int __init demo_init(void)
{
	int ret=0;
	major= register_chrdev(0, NAME,&fops);
	 // If @major == 0 this functions will dynamically allocate a major and return   its number.
	if(major<0)
	{
		printk("register_chrdev fail :%s,%d\n",__func__,__LINE__);
		return EINVAL;
	}
	class=class_create(THIS_MODULE,CLASS_NAME);//创建一个类  在/sys/class下产生一个名字为‘name’的目录
	if(IS_ERR(class))  // IS_ERR(void *ptr);//判断指针的正确与否
	{
		ret=PTR_ERR(class);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to class_create\n");
		goto ERR_STEP1;
		
	}
	device=device_create(class,NULL , MKDEV(major,0), NULL, "%s",NAME) ;  // 利用主次设备号获取设备号：  dev_no = MKDEV(major,minor);
	  // 现象：在/sys/class/(class名称)/(device) 名称下产生一个名字为‘name’的目录
	if(IS_ERR(device))
	{
		ret=PTR_ERR(device);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to device_create\n");
		goto ERR_STEP2;
	}
	printk("demo_init\n");
	return 0;
	ERR_STEP2:
		class_destroy(class);
	ERR_STEP1:
		unregister_chrdev(major, NAME);
	return ret;
}
static void __exit demo_exit(void)
{
	device_destroy(class,MKDEV(major,0));
	class_destroy(class);
	unregister_chrdev(major, NAME);
	printk("demo_exit\n");
}
module_init(demo_init);
module_exit(demo_exit);
MODULE_LICENSE("GPL");
/***********************知识总结*************************
1.Linux系统提供了下面的宏来帮助定义命令:
	 _IO(type,nr)：不带参数的命令
	 _IOR(type,nr,datatype)：从设备中读参数的命令
	 _IOW(type,nr,datatype)：向设备写入参数的命令
	例：
	#define MEM_MAGIC ‘m’ //定义幻数
	#define MEM_SET _IOW(MEM_MAGIC, 0, int)

	1.头文件 (用到这些宏时要包含这个头文件)
    #include <asm-generic/ioctl.h>
    
2.实现操作
unlocked_ioctl函数的实现通常是根据命令执行的一个switch语句。但是，当命令号不能匹配任何一个设备所支持的命令时，返回-EINVAL.
编程模型：
Switch cmd
	Case 命令A：//执行A对应的操作
	Case 命令B：//执行B对应的操作
	Default:// return -EINVAL
3.32bits被分为四部分）类型（8位），序号（8位），参数传送方向（2位），参数长度（数据类型大小）。
    DIR 	size 	     type     nr
    31:30    29:16    8:15      0:7
	 内核中已经被占用的命令码文件： linux-3.14/Documentation/ioctl/unlocked_ioctl 
***********************************************************/








