/*********************
date:2018.1.29
内容：字符设备框架 含自动创建设备节点
**********************/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/err.h>


#define NAME "demo"
dev_t dev=0; //这个是设备号
unsigned baseminor=0; //这个是起始次设备号
unsigned count=5;

#define CLASS_NAME "democlass"
struct class * class=NULL;

struct cdev *cdev=NULL;
struct device *device=NULL;
int major=0,minor=0;

int demo_open (struct inode * inode, struct file *file)
{
	printk("demo_open\n");
	return 0;
}

int demo_release (struct inode *inode, struct file *file)
{
	printk("demo_release\n");
	return 0;
}
 struct file_operations fops={
	.owner=THIS_MODULE,
	.open=demo_open,
	.release=demo_release,
};
static int __init demo_init(void)
{
	int ret=0;
	int i;
	/*字符设备驱动相关逻辑代码*/
	//设备号申请
	ret=alloc_chrdev_region(&dev,  baseminor,  count,NAME);
	if(ret!=0)
	{
		printk("fail to alloc_chrdev_region \n");
		return 0;
	}
	major=MAJOR(dev);
	minor=MINOR(dev);
	printk("major=%d,minor=%d\n",major,minor);
	// 获取主设备号major：	major = MAJOR(dev_no); 获取次设备号minor：	minor = MINOR(dev_no);

	//cdev对象的申请
	cdev=cdev_alloc();
	if(NULL==cdev)
	{
		printk("fail to cdev_alloc");
		goto ERR_STEP1;
	}
	
	//cdev的初始化:[cdev->fops]
	cdev_init(cdev,&fops );
	//cdev注册
	ret= cdev_add( cdev ,  dev,  count);
	if(ret<0)
	{
		printk("fail to cdev_add\n");
	}
	class=class_create(THIS_MODULE,CLASS_NAME);//创建一个类  在/sys/class下产生一个名字为‘name’的目录
/****************全局错误码errno************************
定义：number of last error
在errno.h中定义，全局可见
处理规则：
如果没有出错，则error的值不会被一个例程清除，即只有出错时，才需要检查errno值
任何函数都不会将errno值设置为0，errno.h中定义了所有常数都不为0
***************************************************************/
	if(IS_ERR(class))  // IS_ERR(void *ptr);//判断指针的正确与否
	{
		ret=PTR_ERR(class);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to class_create\n");
		goto ERR_STEP1;
		
	}
	printk("_____dev is :%d\n",dev);
/*****************************************************************************************
struct device *device_create(struct class *class, struct device *parent,dev_t devt, void *drvdata, const char *fmt, ...)
功能：创建对应的device
参数：cls：struct class的指针对象
	parent：设备父对象一般NULL
	dev_t:设备号
	drvdata：设备私有数据
	fmt：设备命名的格式“%s%d”
	...:NAME
返回值：成功：struct device指针对象 失败：NULL
现象：在/sys/class/(class名称)/(device) 名称下产生一个名字为‘name’的目录
********************************************************************************************/
	for(i=0;i<count;i++)
	{
		device=device_create(class,NULL , dev+i, NULL, "%s%d", NAME,i) ;  // 利用主次设备号获取设备号：  dev_no = MKDEV(major,minor);
		  // 现象：在/sys/class/(class名称)/(device) 名称下产生一个名字为‘name’的目录
		if(IS_ERR(device))
		{
			ret=PTR_ERR(device);//PTR_ERR(void *ptr);//将错误指针---->errno
			printk("fail to device_create\n");
			goto ERR_STEP2;
		}
			    
	}
	
	return ret;
ERR_STEP2:
	for(;i>=0;i--)
	{
		device_destroy(class,dev+i);
	}
	class_destroy(class);
	
ERR_STEP1:
	unregister_chrdev_region(dev,count);
	return ret;
}
static void __exit demo_exit(void)
{
	int i;
	for(i=0;i<count;i++)
	{
		device_destroy(class,dev+i);
	}
	class_destroy(class);
	cdev_del(cdev);
	unregister_chrdev_region( dev,  count);//释放设备号
	printk("demo_exit\n");
}
module_init(demo_init);
module_exit(demo_exit);
MODULE_LICENSE("GPL");
/**********************************知识总结*************************************************
1.程序框架
	init()
	{
		设备号申请	alloc_chrdev_region
		cdev对象的申请 cdev_alloc
		cdev的初始化 cdev_init
		cdev注册cdev_add
		创建类 class_create
		在类下面创建设备 device_create
	}
	exit()
	{
		销毁device : device_destroy
		销毁类:   class_destroy
		cdev对象删除 :cdev_del
		释放设备号:  unregister_chrdev_region
	}
2.设备号
	dev_t dev_no
	dev_t :设备号的数据类型
	dev_no ：高12bit主设备号+低20bit次设备号
	获取主设备号major：	major = MAJOR(dev_no);
	获取次设备号minor：	minor = MINOR(dev_no);
	利用主次设备号获取设备号：  dev_no = MKDEV(major,minor);
3.错误处理
	IS_ERR(void *ptr);//判断指针的正确与否
    PTR_ERR(void *ptr);//将错误指针---->errno
4.自动申请设备号alloc_chrdev_region
  手动申请设备号  register_chrdev_region
  register_chrdev 第一个参数为0，则自动分配设备号
*************************************************************************************/
