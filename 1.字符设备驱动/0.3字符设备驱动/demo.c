#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>


#define NAME "DEMO"
dev_t dev=0; //这个是设备号
unsigned baseminor=0; //这个是起始次设备号
unsigned count=1;

struct cdev *cdev=NULL;
int demo_open (struct inode * inode, struct file *file)
{
	printk("demo_open\n");
	return 0;
}

int demo_release (struct inode *inode, struct file *file)
{
	printk("demo_release\n");
	return 0;
}
 struct file_operations fops={
	.owner=THIS_MODULE,
	.open=demo_open,
	.release=demo_release,
};
static int __init demo_init(void)
{
	int ret=0;
	/*字符设备驱动相关逻辑代码*/
	//设备号申请
	ret=alloc_chrdev_region(&dev,  baseminor,  count,NAME);
	if(ret!=0)
	{
		printk("fail to alloc_chrdev_region \n");
		return 0;
	}
	printk("major=%d,minor=%d\n",MAJOR(dev),MINOR(dev));
	
	//cdev对象的申请
	cdev=cdev_alloc();
	if(NULL==cdev)
	{
		printk("fail to cdev_alloc");
		goto ERR_STEP1;
	}
	
	//cdev的初始化:[cdev->fops]
	cdev_init(cdev,&fops );
	//cdev注册
	ret= cdev_add( cdev ,  dev,  count);
	if(ret<0)
	{
		printk("fail to cdev_add\n");
	}

	return 0;
ERR_STEP1:
	unregister_chrdev_region(dev,count);
	return ret;
}
static void __exit demo_exit(void)
{
	cdev_del(cdev);
	unregister_chrdev_region( dev,  count);
	printk("demo_exit\n");
}
module_init(demo_init);
module_exit(demo_exit);
MODULE_LICENSE("GPL");
/*******************************
因为没有自动创建设备节点，所以要手动创建设备节点
mknod /dev/demo c "主设备号" "次设备号"
/*******************************/
