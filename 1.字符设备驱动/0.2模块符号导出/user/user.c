/***************
filename:export.c
date    :2018.2.24
author  :qqx
function:模块符号导出-->使用导出模块的模块
***************/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/export.h>

extern int add(int a, int b);
extern int sub(int a, int b);


int a = 6 ,b = 3;
int  __init  demo_init(void)
{
	printk("add:%d,  sub:%d \n",add(a,b),sub(a,b));
	printk(KERN_EMERG"%s,%d\n",__func__,__LINE__);
	return 0;
}
void __exit demo_exit(void)
{
	printk(KERN_EMERG"%s,%d\n",__func__,__LINE__);
}
module_init(demo_init);
module_exit(demo_exit);
MODULE_LICENSE("GPL");
/***********************知识总结*************************
1.
	(1)假设有3个文件1.c 2.c 3.c ，现在1.c 2.c文件的函数导出给3.c使用
	(2)在分别make1.c 2.c 的时候会产生一个当前文件的符号表文件Module.symvers
		如果当前文件夹下没有的话，有的话就不创建了
	(3)把导出符号的模块生成的文件Module.symvers 中的内容 追加到3.c所在文件夹下Module.symvers文件中，然后make 
	   如果不把要用到的符号导出到这个文件，make的时候会报错
2.	模块插入顺序：先插导出者  再插使用者        【因为导出对应符号之后才能使用】
	模块卸载顺序：先卸载使用者  再再卸载导出者  【因为只有没有使用导出符号的模块时，才能卸载导出者】
***********************************************************/
