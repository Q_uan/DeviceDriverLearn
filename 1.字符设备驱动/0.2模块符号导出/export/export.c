/***************
filename:export.c
date    :2018.2.24
author  :qqx
function:模块符号导出-->要导出内核符号的模块
***************/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/export.h>
int add(int a , int b)
{
	return (a+b);
}
int sub(int a , int b)
{
	if(a > b){
		return (a-b);
	}else{
		return (b-a);
	}
}
EXPORT_SYMBOL(add);
EXPORT_SYMBOL_GPL(sub);

MODULE_LICENSE("GPL");
/***********************知识总结*************************
一.什么是内核符号？为什么要导出模块中的内核符号？
	为了使一个模块中的函数能被另一个函数使用，所以要用内核模块导出，不然会出错。
	安装时按照模块依赖按顺序安装。
二.内核符号的导出使用宏
	EXPORT_SYMBOL(符号名)
	EXPORT_SYMBOL_GPL(符号名)
	EXPORT_SYMBOL_GPL_FUTURE(待导出的符号);
		其中EXPORT_SYMBOL_GPL只能用于包含GPL许可证的模块
***********************************************************/