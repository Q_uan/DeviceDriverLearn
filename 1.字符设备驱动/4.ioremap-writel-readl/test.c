#include<stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <string.h>
#include <stdlib.h>
#include "led_ioctl.h"

#define NAME "/dev/demo"
 int main(int argc, const char *argv[])
{
	int ret;
	int i=0;
	if(argc!=2)
	{
		printf("usage: filename ON/OFF\n");
		exit(1);
	}
	int fd=open(NAME,O_RDWR);
	if(fd<0)
	{
		perror("open");//void perror(const char *s);  perror——输出用户信息及errno对应的错误信息
	}
	if(0==strncmp(argv[1],"ON",2))
	{
		ret=ioctl(fd,ON);
		if(0!=ret)
		{
			perror("ioctl");
		}
	}
	if(0==strncmp(argv[1],"OFF",3))
	{
		ret=ioctl(fd,OFF);
		if(0!=ret)
		{
			perror("ioctl");
		}
	}

	close(fd);
	return 0;
}
/***********************知识总结*************************

**********************************************************/
