#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/device.h>
#include <linux/io.h>

#include "led_ioctl.h"
#define GPF3CON  0x114001E0
#define GPF3DAT  0x114001E4
unsigned int* gpf3con =NULL;
unsigned int* gpf3dat =NULL;
#define NAME "demo"
int major;
#define CLASS_NAME "democlass"
struct class * class;
struct device * device;
int demo_open(struct inode *inode , struct file *file)
{
	printk("demo_open\n");
	return 0;
}
int demo_release  (struct inode *inode, struct file *file)
{
	printk("demo_release\n");
	return 0;
}
long demo_ioctl (struct file *file, unsigned int cmd , unsigned long arg)
{
	switch(cmd)
	{
		case ON:
			writel(readl(gpf3dat)|(0x1<<5),gpf3dat);
			printk("LED ON\n");
			break;
		case OFF:
			writel(readl(gpf3dat)&(~(0x1<<5)),gpf3dat);
			printk("LED OFF\n");
			break;
		default:
			printk("command default\n");
			break;	
	}
	return 0;
}
const struct file_operations fops={
	.owner=THIS_MODULE,
	.open=demo_open,
	.release=demo_release,
	.unlocked_ioctl=demo_ioctl,
};
static int __init demo_init(void)
{
	int ret=0;
	major= register_chrdev(0, NAME,&fops);
	 // If @major == 0 this functions will dynamically allocate a major and return   its number.
	if(major<0)
	{
		printk("register_chrdev fail :%s,%d\n",__func__,__LINE__);
		return EINVAL;
	}
	class=class_create(THIS_MODULE,CLASS_NAME);//创建一个类  在/sys/class下产生一个名字为‘name’的目录
	if(IS_ERR(class))  // IS_ERR(void *ptr);//判断指针的正确与否
	{
		ret=PTR_ERR(class);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to class_create\n");
		goto ERR_STEP1;
		
	}
	device=device_create(class,NULL , MKDEV(major,0), NULL, "%s",NAME) ;  // 利用主次设备号获取设备号：  dev_no = MKDEV(major,minor);
	  // 现象：在/sys/class/(class名称)/(device) 名称下产生一个名字为‘name’的目录
	if(IS_ERR(device))
	{
		ret=PTR_ERR(device);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to device_create\n");
		goto ERR_STEP2;
	}
	/********************完成映射:物理--->虚拟*****************************/
	gpf3con=ioremap(GPF3CON, 4);
	if(NULL==gpf3con)
	{
		printk("fail to ioremap :%s,%d\n",__func__,__LINE__);
		goto ERR_STEP2;
	}
	gpf3dat=ioremap(GPF3DAT, 4);
	if(NULL==gpf3dat)
	{
		printk("fail to ioremap :%s,%d\n",__func__,__LINE__);
		goto ERR_STEP3;
	}
	writel( (readl(gpf3con)&(~(0xf<<20)) )|(0x1<<20),gpf3con);
	writel(readl(gpf3dat)&(~(0x1<<5)),gpf3dat);
	printk("demo_init\n");
	return 0;

	ERR_STEP3:
		iounmap(gpf3con);
	ERR_STEP2:
		class_destroy(class);
	ERR_STEP1:
		unregister_chrdev(major, NAME);
	return ret;
}
static void __exit demo_exit(void)
{
	iounmap(gpf3con);
	iounmap(gpf3dat);
	device_destroy(class,MKDEV(major,0));
	class_destroy(class);
	unregister_chrdev(major, NAME);
	printk("demo_exit\n");
}
module_init(demo_init);
module_exit(demo_exit);
MODULE_LICENSE("GPL");
/***********************知识总结*************************
1.相关函数
	void __iomem *ioremap(unsigned long paddr, unsigned long size)
	功能：映射物理地址--》虚拟地址
	参数：paddr：物理地址
	            size：映射大小
	返回值：成功：返回该物理地址对应的虚拟地址 失败：NULL

	static inline void iounmap(volatile void __iomem *addr)
	功能：解除映射关系
	参数：addr：虚拟地址
	返回值：无

	static inline u32 readl(const volatile void __iomem *addr)
	功能：读取虚拟地址中的数据
	参数：addr：虚拟地址
	返回值：对应的数据

	static inline void writel(u32 b, volatile void __iomem *addr)
	功能：像虚拟地址中写入指定的数据
	参数：b：待写入的数据
	            addr：虚拟地址
	返回值：无
2.程序编写：led灯驱动
	[1]查看原理图--->led的控制管脚
	    GPF3_5
	[2]根据对应的管脚查看芯片手册，去配置该管脚的功能及使用
	#define GPF3CON 0X114001E0 --> OUTPUT
	#define GPF3DAT 0X114001E4 --> value
	[3]写驱动
	    [3.1]将对应的io管脚控制的物理地址进行相关映射
	        virt_addr = ioremap(GPF3CON);
	    [3.2]完成配置
	    	 readl、writel
	    [3.3]实现功能
			led：IO---->设置对应的IO模式：输入、输出、中断。。。
			        GPF3_5： 23:20 ---> 0x1 --->output
			        arm：读 改 写
					读取23:20--->readl--->data
					改：23:20--->data  :x x x x ---->data = readl(gpf3con);
					                    8 4 2 1	
					            清零：&~(0xf<<20)------->data&~(0xf<<20)				            									  
					写：23:20--->data:writel((0x1<<20) , gpf3con);
***********************************************************/








