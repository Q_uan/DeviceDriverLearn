#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/moduleparam.h>

/*传递指定类型的参数*/
int num =5;
module_param(num,int,0775);
MODULE_PARM_DESC(num,"int num module_param");
char *str=NULL;
module_param(str,charp,0775);
MODULE_PARM_DESC(str,"char str module_param");


/*传递参数内外部参数名称不一致时*/
int buf_out;
int buf_in = 5;
module_param_named(buf_out,buf_in,int,0775);
MODULE_PARM_DESC(buf_out,"buf_out-->buf_in  module_param_named");

/*传递字符串到内部的全局字符数组内*/
char string[20] = {0};
module_param_string(string,string,sizeof(string),0775);
MODULE_PARM_DESC(string,"string  module_param_string");

/*传递数组型参数*/
int array[5] = {1,2,3,4,5};
int size= 5;
module_param_array(array,int,&size,0775);
MODULE_PARM_DESC(array," module_param_array");

int  __init  demo_init(void)
{
	int i = 0;
	printk("num:%d \n",num);
	printk("str:%s \n",str);
	printk("buf_in:%d \n",buf_in);
	printk("string:%s \n",string);
	for(i = 0 ; i < size ; i++)
		printk("array[%d]:%d \n", i, array[i]);
	printk("%s,%d\n",__func__,__LINE__);
	return 0;
}

void __exit demo_exit(void)
{
	printk(KERN_EMERG"%s,%d\n",__func__,__LINE__);
}

module_init(demo_init);
module_exit(demo_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("QuanQixian");
MODULE_DESCRIPTION("测试模块参数");
MODULE_VERSION("V1.0");


/***********************知识总结*************************
1.模块申明
	1、MODULE_LICENSE（”遵守的协议”）
		申明该模块遵守的许可证协议，如：“GPL“（开源协议）、”GPL v2“等
	2、MODULE_AUTHOR(“作者”)
		申明模块的作者
	3、MODULE_DESCRIPTION(“模块的功能描述")
		申明模块的功能
	4、MODULE_VERSION("V1.0")
		申明模块的版本
	5.MODULE_PARAM_DESC(对应的参数名称,“参数描述信息”)
		模块参数的描述信息
2.模块参数   内核通过在加载驱动的模块时指定可变参数的值
	参数类型:
		bool,invbool,charp(字符串),int,long,short,uint,ulong,ushort;
	使用方法	
		1.传递指定类型的变量[全局]
		module_param(“参数的名称”, type参数的类型, 参数的权限);
		2.传递参数内外部参数名称不一致时:
		module_param_named(外部传递参数的名称, 内部接收的参数的名称, 参数类型, 参数的权限);
		3.传递字符串到内部的全局字符数组内
		module_param_string(外部传递字符串的参数名称, 内部接收字符串的参数名称, 字符串的长度, 参数的权限);
		4.传递数组型参数   数组命令行传参用“,”隔开例：array=100,200,300,400,500
		module_param_array(数组名称, 数组成员的类型, 外部向内部模块传递的数组成员的个数, 参数的权限);
	传参实例
		root@Audrey ]# insmod demo.ko num=100 str="hello" buf_out=100 string="world" array=100,200,300,400,500
		[  920.810000] num:100 
		[  920.810000] str:hello 
		[  920.815000] buf_in:100 
		[  920.815000] string:world 
		[  920.820000] array[0]:100 
		[  920.820000] array[1]:200 
		[  920.825000] array[2]:300 
		[  920.825000] array[3]:400 
		[  920.830000] array[4]:500 
		[  920.830000] demo_init,41
		
*****************************************************************************************/
