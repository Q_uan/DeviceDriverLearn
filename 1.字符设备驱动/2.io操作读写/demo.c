#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/device.h>
#define NAME "demo"
int major;
#define MAX 64
char buf[MAX]={9,8,7,6,5,4,3,8,8};
#define CLASS_NAME "democlass"
struct class * class;
struct device * device;
ssize_t demo_read (struct file *file, char __user * user, size_t size, loff_t *loff)
{
	int ret;
	if(size>MAX)
	size=MAX;
	/*
	unsigned long copy_to_user(void __user *to, const void *from, unsigned long n)
    功能：将内核的数据传递给用户空间
    参数:	to:用户空间的地址
       	  from：内核空间的指针
          n:传递的数据的大小
    返回值：成功： 0 失败：传输的数据的个数
    */
	ret= copy_to_user(user, buf, size);//将内核的数据传递给用户空间
	if(ret)
	{
		printk("fail to copy_to _user\n");
		return -EAGAIN;
	}
	printk("demo_read\n");
	return size;
}
ssize_t demo_write (struct file *file, const char __user *user, size_t size, loff_t *loff)
{
	int ret;
	if(size>MAX)
		size=MAX;
	ret= copy_from_user(buf, user,size);
	if(ret)
	{
		printk("fail to copy_from_user\n");
		return -EAGAIN;
	}
	printk("demo_write\n");
	return size;
}
int demo_open(struct inode *inode , struct file *file)
{
	printk("demo_open\n");
	return 0;
}
int demo_release  (struct inode *inode, struct file *file)
{
	printk("demo_release\n");
	return 0;
}
const struct file_operations fops={
	.owner=THIS_MODULE,
	.open=demo_open,
	.release=demo_release,
	.read=demo_read,
	.write=demo_write,
};

static int __init demo_init(void)
{
	int ret=0;
	major= register_chrdev(0, NAME,&fops);
	 // If @major == 0 this functions will dynamically allocate a major and return   its number.
	if(major<0)
	{
		printk("register_chrdev fail :%s,%d\n",__func__,__LINE__);
		return EINVAL;
	}
	class=class_create(THIS_MODULE,CLASS_NAME);//创建一个类  在/sys/class下产生一个名字为‘name’的目录
	if(IS_ERR(class))  // IS_ERR(void *ptr);//判断指针的正确与否
	{
		ret=PTR_ERR(class);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to class_create\n");
		goto ERR_STEP1;
		
	}
	device=device_create(class,NULL , MKDEV(major,0), NULL, "%s",NAME) ;  // 利用主次设备号获取设备号：  dev_no = MKDEV(major,minor);
	  // 现象：在/sys/class/(class名称)/(device) 名称下产生一个名字为‘name’的目录
	if(IS_ERR(device))
	{
		ret=PTR_ERR(device);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to device_create\n");
		goto ERR_STEP2;
	}
	printk("demo_init\n");
	return 0;
	ERR_STEP2:
		class_destroy(class);
	ERR_STEP1:
		unregister_chrdev(major, NAME);
	return ret;
	

}
static void __exit demo_exit(void)
{
	device_destroy(class,MKDEV(major,0));
	class_destroy(class);
	unregister_chrdev(major, NAME);
	printk("demo_exit\n");
}
module_init(demo_init);
module_exit(demo_exit);
MODULE_LICENSE("GPL");
/***********************知识总结*************************
1.程序框架
	init()
	{
		register_chrdev 第一个参数为0，则自动分配设备号
	}
	exit()
	{
		unregister_chrdev
	}
	区别：
	alloc_chrdev_region是从baseminor作为次设备号开始连续创建count个设备号
	register_chrdev是从0作为次设备号开始连续创建256个设备号
	可以通过追源码知道
	
	register_chrdev等于把
			*设备号申请	alloc_chrdev_region
			*cdev对象的申请 cdev_alloc
			*cdev的初始化 cdev_init
			*cdev注册cdev_add
	四步合在一起了
2.IO读写	
	#include <asm/uaccess.h>
	READ:
		ker_date ----	copy_to_user  ----》user
		unsigned long copy_to_user(void __user *to, const void *from, unsigned long n)
		功能：将内核的数据传递给用户空间
		参数：to:用户空间的地址
			  from：内核空间的指针
			  n:传递的数据的大小
		返回值：成功： 0   失败：传输的数据的个数
	
	write:
		unsigned long copy_from_user(void *to, const void __user *from, unsigned long n)
		功能：将用户的数据传递给内核空间
		参数：to:内核空间的地址
			  from：用户空间的指针
			  n:传递的数据的大小
		返回值：成功： 0   失败：传输的数据的个数
***********************************************************/








