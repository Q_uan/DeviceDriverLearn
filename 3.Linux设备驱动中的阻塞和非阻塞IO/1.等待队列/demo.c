/***************
filename:demo.c
date    :2018.3.1
author  :qqx
function:
实现系统调用的read阻塞，read 无数据则等待 直到有数据
原理：使用等待队列相关的机制
步骤：
1.定义 初始化等待队列头
2.判断条件
	如果条件不满足，阻塞等待 -->唤醒队列
***************/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/device.h>
#include <linux/wait.h>
#include <linux/sched.h>
#define NAME "demo"
int major;
#define CLASS_NAME "democlass"
struct class * class;
struct device * device;

wait_queue_head_t wq;
int flags=0;   //write后=1 read后=0

#define MAX 64
char ker_buf[MAX]={0};
int demo_open(struct inode *inode , struct file *file)
{
	if(!mutex_trylock(&mutex))
	{
		printk("can not get mutex\n");
		return -EBUSY;
	}
	printk("demo_open\n");
	return 0;
}
int demo_release  (struct inode *inode, struct file *file)
{
	mutex_unlock(&mutex);
	printk("demo_release\n");
	return 0;
}
const struct file_operations fops={
	.owner=THIS_MODULE,
	.open=demo_open,
	.release=demo_release,
};
ssize_t demo_read (struct file *file, char __user *user, size_t, loff_t *loff)
{
	int ret=0;
	if(size>MAX) SIZE=MAX;
	if(!flags)//条件不满足
	{
		if(wait_event_interruptible(wq,flags))//被信号大打断返回非0值，被正常唤醒
			return  ERESTARTSYS;
	}
	ret= copy_to_user(user, ker_buf, size);//将内核的数据传递给用户空间
	if(ret)
	{
		printk("fail to copy_to _user\n");
		return -EAGAIN;
	}
	flags=0;
	msmset(ker_buf,0,MAX);
	return size;
}
ssize_t demo_write (struct file *, const char __user *user, size_t, loff_t *loff)
{
	if(size>MAX) size=MAX;
	if(copy_from_user(ker_buf,user,size))
	{
		printk("copy_from_user fail \n");
		return -EINVAL;
	}
	flags=1;
	wake_up(&wq);
	return 0;
}


static int __init demo_init(void)
{
	int ret=0;
	major= register_chrdev(0, NAME,&fops);
	if(major<0)
	{
		printk("register_chrdev fail :%s,%d\n",__func__,__LINE__);
		return EINVAL;
	}
	class=class_create(THIS_MODULE,CLASS_NAME);//创建一个类  在/sys/class下产生一个名字为‘name’的目录
	if(IS_ERR(class))  // IS_ERR(void *ptr);//判断指针的正确与否
	{
		ret=PTR_ERR(class);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to class_create\n");
		goto ERR_STEP1;	
	}
	device=device_create(class,NULL , MKDEV(major,0), NULL, "%s",NAME) ;  // 利用主次设备号获取设备号：  dev_no = MKDEV(major,minor);
	  // 现象：在/sys/class/(class名称)/(device) 名称下产生一个名字为‘name’的目录
	if(IS_ERR(device))
	{
		ret=PTR_ERR(device);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to device_create\n");
		goto ERR_STEP2;
	}
	init_waitqueue_head(&wq);
	printk("demo_init\n");
	return 0;
	ERR_STEP2:
		class_destroy(class);
	ERR_STEP1:
		unregister_chrdev(major, NAME);
	return ret;
}
static void __exit demo_exit(void)
{
	device_destroy(class,MKDEV(major,0));
	class_destroy(class);
	unregister_chrdev(major, NAME);
	printk("demo_exit\n");
}
module_init(demo_init);
module_exit(demo_exit);
MODULE_LICENSE("GPL");
/***********************知识总结*************************
1.相关操作
	1．定义“等待队列头”。
		wait_queue_head_t my_queue;
	2．初始化“等待队列头”。
	init_waitqueue_head(&my_queue);
	而下面的 DECLARE_WAIT_QUEUE_HEAD()宏可以作为定义并初始化等待队列头的“快捷方式” 。
		DECLARE_WAIT_QUEUE_HEAD (name)
	3．定义等待队列。
	DECLARE_WAITQUEUE(name, tsk)
	该宏用于定义并初始化一个名为 name 的等待队列。
	4．添加/移除等待队列。
		void fastcall add_wait_queue(wait_queue_head_t *q, wait_queue_t *wait);
		void fastcall remove_wait_queue(wait_queue_head_t *q, wait_queue_t *wait);
		
		add_wait_queue()用于将等待队列 wait 添加到等待队列头 q 指向的等待队列链表
		中， 而 remove_wait_queue()用于将等待队列 wait 从附属的等待队列头 q 指向的等待队
		列链表中移除。
	5．等待事件。
		wait_event(queue, condition)
		wait_event_interruptible(queue, condition)
		wait_event_timeout(queue, condition, timeout)
		wait_event_interruptible_timeout(queue, condition, timeout)
			等待第一个参数 queue 作为等待队列头的等待队列被唤醒，而且第二个参数
			condition 必须满足，否则阻塞。wait_event()和 wait_event_interruptible()的区别在于后
			者可以被信号打断，而前者不能。加上_timeout 后的宏意味着阻塞等待的超时时间，
			以 jiffy 为单位，在第三个参数的 timeout 到达时，不论 condition 是否满足，均返回。
			wait()的定义如代码清单 8.3 所示，从其源代码可以看出，当 condition 满足时，wait_event()会立即返回，否则，阻塞等待 condition 满足。
	6．唤醒队列。
		void wake_up(wait_queue_head_t *queue);
		void wake_up_interruptible(wait_queue_head_t *queue);
		上述操作会唤醒以 queue 作为等待队列头的所有等待队列中所有属于该等待队列头的等待队列对应的进程。
	wake_up() 应 与 wait_event() 或 wait_event_timeout() 成 对 使 用 ， 而
	wake_up_interruptible() 则 应 与 wait_event_interruptible() 或
	wait_event_interruptible_timeout() 成 对 使 用 。 wake_up() 可 唤 醒 处 于
	TASK_INTERRUPTIBLE 和 TASK_UNINTERRUPTIBLE 的 进 程 ， 而
	wake_up_interruptible()只能唤醒处于 TASK_INTERRUPTIBLE 的进程。	
		7．在等待队列上睡眠。
	sleep_on(wait_queue_head_t *q );
	interruptible_sleep_on(wait_queue_head_t *q );
***********************************************************/








