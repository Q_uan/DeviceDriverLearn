/***************
filename:demo.c
date    :2018.3.1
author  :qqx
function:使用互斥体实现设备只能被一个进程打开
***************/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/semaphore.h> 
#include <linux/device.h>

#define NAME "demo"
int major;
#define CLASS_NAME "democlass"
struct class * class;
struct device * device;

struct mutex mutex;
int demo_open(struct inode *inode , struct file *file)
{
	if(!mutex_trylock(&mutex))
	{
		printk("can not get mutex\n");
		return -EBUSY;
	}
	printk("demo_open\n");
	return 0;
}
int demo_release  (struct inode *inode, struct file *file)
{
	mutex_unlock(&mutex);
	printk("demo_release\n");
	return 0;
}
const struct file_operations fops={
	.owner=THIS_MODULE,
	.open=demo_open,
	.release=demo_release,
};
static int __init demo_init(void)
{
	int ret=0;
	major= register_chrdev(0, NAME,&fops);
	if(major<0)
	{
		printk("register_chrdev fail :%s,%d\n",__func__,__LINE__);
		return EINVAL;
	}
	class=class_create(THIS_MODULE,CLASS_NAME);//创建一个类  在/sys/class下产生一个名字为‘name’的目录
	if(IS_ERR(class))  // IS_ERR(void *ptr);//判断指针的正确与否
	{
		ret=PTR_ERR(class);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to class_create\n");
		goto ERR_STEP1;	
	}
	device=device_create(class,NULL , MKDEV(major,0), NULL, "%s",NAME) ;  // 利用主次设备号获取设备号：  dev_no = MKDEV(major,minor);
	  // 现象：在/sys/class/(class名称)/(device) 名称下产生一个名字为‘name’的目录
	if(IS_ERR(device))
	{
		ret=PTR_ERR(device);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to device_create\n");
		goto ERR_STEP2;
	}
	mutex_init(&mutex);
	printk("demo_init\n");
	return 0;
	ERR_STEP2:
		class_destroy(class);
	ERR_STEP1:
		unregister_chrdev(major, NAME);
	return ret;
}
static void __exit demo_exit(void)
{
	device_destroy(class,MKDEV(major,0));
	class_destroy(class);
	unregister_chrdev(major, NAME);
	printk("demo_exit\n");
}
module_init(demo_init);
module_exit(demo_exit);
MODULE_LICENSE("GPL");
/***********************知识总结*************************
1.相关操作
	定义互斥体 struct mutex my_mutex;
	初始化       mutex_init(&my_mutex);
	获取互斥体
		void fastcall mutex_lock(struct mutex *lock);
		int fastcall mutex_lock_interruptible(struct mutex *lock);
		int fastcall mutex_trylock(struct mutex *lock);
		
		mutex_lock()与 mutex_lock_interruptible()的区别和 down()与 down_trylock()的区别
		完全一致，前者引起的睡眠不能被信号打断，而后者可以。mutex_trylock()用于尝试
		获得 mutex，获取不到 mutex 时不会引起进程睡眠。
	释放互斥体
		void fastcall mutex_unlock(struct mutex *lock);
2.使用示例 mutex 的使用方法和信号量用于互斥的场合完全一样
	struct mutex my_mutex; //定义 mutex
	mutex_init(&my_mutex); //初始化 mutex
	mutex_lock(&my_mutex); //获取 mutex
	...//临界资源
	mutex_unlock(&my_mutex); //释放 mutex
	***********************************************************/








