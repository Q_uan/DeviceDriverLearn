/***************
filename:demo.c
date    :2018.3.1
author  :qqx
function:使用信号量实现设备只能被一个进程打开
***************/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/semaphore.h> 
#include <linux/device.h>

#define NAME "demo"
int major;
#define CLASS_NAME "democlass"
struct class * class;
struct device * device;

struct semaphore sem;
int demo_open(struct inode *inode , struct file *file)
{
	if(down_trylock(&sem))
	{
		printk("can not opne\n");
		return -EBUSY;
	}
	
	printk("demo_open\n");
	return 0;
}
int demo_release  (struct inode *inode, struct file *file)
{
	up(&sem);
	printk("demo_release\n");
	return 0;
}
const struct file_operations fops={
	.owner=THIS_MODULE,
	.open=demo_open,
	.release=demo_release,
};
static int __init demo_init(void)
{
	int ret=0;
	major= register_chrdev(0, NAME,&fops);
	if(major<0)
	{
		printk("register_chrdev fail :%s,%d\n",__func__,__LINE__);
		return EINVAL;
	}
	class=class_create(THIS_MODULE,CLASS_NAME);//创建一个类  在/sys/class下产生一个名字为‘name’的目录
	if(IS_ERR(class))  // IS_ERR(void *ptr);//判断指针的正确与否
	{
		ret=PTR_ERR(class);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to class_create\n");
		goto ERR_STEP1;	
	}
	device=device_create(class,NULL , MKDEV(major,0), NULL, "%s",NAME) ;  // 利用主次设备号获取设备号：  dev_no = MKDEV(major,minor);
	  // 现象：在/sys/class/(class名称)/(device) 名称下产生一个名字为‘name’的目录
	if(IS_ERR(device))
	{
		ret=PTR_ERR(device);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to device_create\n");
		goto ERR_STEP2;
	}
	sema_init(&sem,2);//初始化信号量
	printk("demo_init\n");
	return 0;
	ERR_STEP2:
		class_destroy(class);
	ERR_STEP1:
		unregister_chrdev(major, NAME);
	return ret;
}
static void __exit demo_exit(void)
{
	device_destroy(class,MKDEV(major,0));
	class_destroy(class);
	unregister_chrdev(major, NAME);
	printk("demo_exit\n");
}
module_init(demo_init);
module_exit(demo_exit);
MODULE_LICENSE("GPL");
/***********************知识总结*************************
1.	信号量（semaphore）是用于保护临界区的一种常用方法，它的使用方式和自旋锁
	类似。与自旋锁相同，只有得到信号量的进程才能执行临界区代码。但是，与自旋锁
	不同的是，当获取不到信号量时，进程不会原地打转而是进入休眠等待状态。
2.相关操作
	1．定义信号量 struct semaphore sem;
	2．初始化信号量 void sema_init (struct semaphore *sem, int val);
		该函数初始化信号量，并设置信号量 sem 的值为 val。尽管信号量可以被初始化
		为大于 1 的值从而成为一个计数信号量，但是它通常不被这样使用。

		void init_MUTEX(struct semaphore *sem);
			该函数用于初始化一个用于互斥的信号量，它把信号量 sem 的值设置为 1，等同于sema_init (struct semaphore *sem, 1)。
		void init_MUTEX_LOCKED (struct semaphore *sem);
			该函数也用于初始化一个信号量，但它把信号量 sem 的值设置为 0，等同于sema_init (struct semaphore *sem, 0)。
		此外，下面两个宏是定义并初始化信号量的“快捷方式” 。
			DECLARE_MUTEX(name)
			DECLARE_MUTEX_LOCKED(name)
			前者定义一个名为 name 的信号量并初始化为 1，后者定义一个名为 name 的信号量
			并初始化为 0。
	3．获得信号量
		void down(struct semaphore * sem);
			该函数用于获得信号量 sem，它会导致睡眠，因此不能在中断上下文使用。
		int down_interruptible(struct semaphore * sem);
			该函数功能与 down()类似，不同之处为，因为 down()而进入睡眠状态的进程不能
			被信号打断，而因为 down_interruptible()而进入睡眠状态的进程能被信号打断，信号
			也会导致该函数返回，这时候函数的返回值非 0。
		int down_trylock(struct semaphore * sem);
			该函数尝试获得信号量 sem，如果能够立刻获得，它就获得该信号量并返回 0，
			否则，返回非 0 值。它不会导致调用者睡眠，可以在中断上下文使用。
			
			在使用 down_interruptible()获取信号量时，对返回值一般会进行检查，如果非 0，
			通常立即返回-ERESTARTSYS，如：
			if (down_interruptible(&sem))
			{
			return - ERESTARTSYS;
			}
	4．释放信号量  void up(struct semaphore * sem); 该函数释放信号量 sem，唤醒等待者。
3.使用示例
	DECLARE_MUTEX(mount_sem);
	down(&mount_sem);//获取信号量，保护临界区
	...
	critical section //临界区
	...
	up(&mount_sem);//释放信号量
4.信号量用于同步
	如果信号量被初始化为 0，则它可以用于同步，同步意味着一个执行单元的继续
	执行需等待另一执行单元完成某事，保证执行的先后顺序。如图 7.4 所示，执行单元
	A 执行代码区域 b 之前，必须等待执行单元 B 执行完代码单元 c，信号量可辅助这一
	同步过程。
***********************************************************/








