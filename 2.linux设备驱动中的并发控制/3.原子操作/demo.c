/***************
filename:demo.c
date    :2018.3.1
author  :qqx
function:使用原子变量使设备只能被一个进程打开
***************/
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/device.h>

#define NAME "demo"
int major;
#define CLASS_NAME "democlass"
struct class * class;
struct device * device;

atomic_t at=ATOMIC_INIT(1);
int demo_open(struct inode *inode , struct file *file)
{
	if(!atomic_dec_and_test(&at))//上述操作对原子变量执行自减操作后（注意没有加）测试其是否为 0，为 0 则返回 true，否则返回 false。
	{
		atomic_inc(&at);
		retrurn -EBUSY;
	}
	printk("demo_open\n");
	return 0;
}
int demo_release  (struct inode *inode, struct file *file)
{
	atomic_inc(&at);
	printk("demo_release\n");
	return 0;
}
const struct file_operations fops={
	.owner=THIS_MODULE,
	.open=demo_open,
	.release=demo_release,
};
static int __init demo_init(void)
{
	int ret=0;
	major= register_chrdev(0, NAME,&fops);
	if(major<0)
	{
		printk("register_chrdev fail :%s,%d\n",__func__,__LINE__);
		return EINVAL;
	}
	class=class_create(THIS_MODULE,CLASS_NAME);//创建一个类  在/sys/class下产生一个名字为‘name’的目录
	if(IS_ERR(class))  // IS_ERR(void *ptr);//判断指针的正确与否
	{
		ret=PTR_ERR(class);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to class_create\n");
		goto ERR_STEP1;	
	}
	device=device_create(class,NULL , MKDEV(major,0), NULL, "%s",NAME) ;  // 利用主次设备号获取设备号：  dev_no = MKDEV(major,minor);
	  // 现象：在/sys/class/(class名称)/(device) 名称下产生一个名字为‘name’的目录
	if(IS_ERR(device))
	{
		ret=PTR_ERR(device);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to device_create\n");
		goto ERR_STEP2;
	}
	mutex_init(&mutex);
	printk("demo_init\n");
	return 0;
	ERR_STEP2:
		class_destroy(class);
	ERR_STEP1:
		unregister_chrdev(major, NAME);
	return ret;
}
static void __exit demo_exit(void)
{
	device_destroy(class,MKDEV(major,0));
	class_destroy(class);
	unregister_chrdev(major, NAME);
	printk("demo_exit\n");
}
module_init(demo_init);
module_exit(demo_exit);
MODULE_LICENSE("GPL");
/***********************知识总结*************************
1.原子操作指的是在执行过程中不会被别的代码路径所中断的操作。
	Linux 内核提供了一系列函数来实现内核中的原子操作，这些函数又分为两类，
	分别针对【位】和【整型变量】进行原子操作。它们的共同点是在任何情况下操作都是原子
	的，内核代码可以安全地调用它们而不被打断。位和整型变量原子操作都依赖底层
	CPU 的原子操作来实现，因此所有这些函数都与 CPU 架构密切相关。
2.整型原子操作
	1．设置原子变量的值
	void atomic_set(atomic_t *v, int i); //设置原子变量的值为 i
	atomic_t v = ATOMIC_INIT(0); //定义原子变量 v 并初始化为 0
	2．获取原子变量的值
	atomic_read(atomic_t *v); //返回原子变量的值
	3．原子变量加/减
	void atomic_add(int i, atomic_t *v); //原子变量增加 i
	void atomic_sub(int i, atomic_t *v); //原子变量减少 i
	4．原子变量自增/自减
	void atomic_inc(atomic_t *v); //原子变量增加 1
	void atomic_dec(atomic_t *v); //原子变量减少 1
	5．操作并测试
	int atomic_inc_and_test(atomic_t *v);
	int atomic_dec_and_test(atomic_t *v);
	int atomic_sub_and_test(int i, atomic_t *v);
	上述操作对原子变量执行自增、自减和减操作后（注意没有加）测试其是否为 0，
	为 0 则返回 true，否则返回 false。
	6．操作并返回
	int atomic_add_return(int i, atomic_t *v);
	int atomic_sub_return(int i, atomic_t *v);
	int atomic_inc_return(atomic_t *v);
	int atomic_dec_return(atomic_t *v);
	上述操作对原子变量进行加/减和自增/自减操作，并返回新的值。
3.位原子操作
	1．设置位
	void set_bit(nr, void *addr);
	上述操作设置 addr 地址的第 nr 位，所谓设置位即将位写为 1。
	2．清除位
	void clear_bit(nr, void *addr);
	上述操作清除 addr 地址的第 nr 位，所谓清除位即将位写为 0。
	3．改变位
	void change_bit(nr, void *addr);
	上述操作对 addr 地址的第 nr 位进行反置。
	4．测试位
	test_bit(nr, void *addr);
	上述操作返回 addr 地址的第 nr 位。
	5．测试并操作位
	int test_and_set_bit(nr, void *addr);
	int test_and_clear_bit(nr, void *addr);
	int test_and_change_bit(nr, void *addr);
	上述 test_and_xxx_bit（nr, void *addr）操作等同于执行 test_bit（nr, void *addr）后
	再执行 xxx_bit（nr, void *addr） 。
***********************************************************/








