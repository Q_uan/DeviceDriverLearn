#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/device.h>
#include <linux/spinlock.h>

#define NAME "demo"
int major;
#define CLASS_NAME "democlass"
struct class * class;
struct device * device;

spinlock_t lock;
int flag=0;
int demo_open(struct inode *inode , struct file *file)
{
	spin_lock(&lock);
	if(flag)
	{
		spin_unlock(&lock);
		printk("this device open already\n");
		return -EBUSY;
	}
	flag=1;
	spin_unlock(&lock);
	printk("demo_open\n");
	return 0;
}
int demo_release  (struct inode *inode, struct file *file)
{
	spin_lock(&lock);
	flag=0;
	spin_unlock(&lock);
	printk("demo_release\n");
	return 0;
}
const struct file_operations fops={
	.owner=THIS_MODULE,
	.open=demo_open,
	.release=demo_release,
};
static int __init demo_init(void)
{
	int ret=0;
	major= register_chrdev(0, NAME,&fops);
	if(major<0)
	{
		printk("register_chrdev fail :%s,%d\n",__func__,__LINE__);
		return EINVAL;
	}
	class=class_create(THIS_MODULE,CLASS_NAME);//创建一个类  在/sys/class下产生一个名字为‘name’的目录
	if(IS_ERR(class))  // IS_ERR(void *ptr);//判断指针的正确与否
	{
		ret=PTR_ERR(class);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to class_create\n");
		goto ERR_STEP1;	
	}
	device=device_create(class,NULL , MKDEV(major,0), NULL, "%s",NAME) ;  // 利用主次设备号获取设备号：  dev_no = MKDEV(major,minor);
	  // 现象：在/sys/class/(class名称)/(device) 名称下产生一个名字为‘name’的目录
	if(IS_ERR(device))
	{
		ret=PTR_ERR(device);//PTR_ERR(void *ptr);//将错误指针---->errno
		printk("fail to device_create\n");
		goto ERR_STEP2;
	}
	spin_lock_init(&lock);
	printk("demo_init\n");
	return 0;
	ERR_STEP2:
		class_destroy(class);
	ERR_STEP1:
		unregister_chrdev(major, NAME);
	return ret;
}
static void __exit demo_exit(void)
{
	device_destroy(class,MKDEV(major,0));
	class_destroy(class);
	unregister_chrdev(major, NAME);
	printk("demo_exit\n");
}
module_init(demo_init);
module_exit(demo_exit);
MODULE_LICENSE("GPL");
/***********************知识总结*************************
1.自旋锁（spin lock）是一种对临界资源进行互斥手访问的典型手段，其名称来源于它的工作方式。
	为了获得一个自旋锁，在某 CPU 上运行的代码需先执行一个原子操作，
	该操作测试并设置（test-and-set）某个内存变量，由于它是原子操作
	，所以在该操作完成之前其他执行单元不可能访问这个内存变量。
	
	如果测试结果表明锁已经空闲，则程序获得这个自旋锁并继续执行；如果测试结
	果表明锁仍被占用，程序将在一个小的循环内重复这个“测试并设置”操作，即进行
	所谓的“自旋” ，通俗地说就是“在原地打转” 。当自旋锁的持有者通过重置该变量释
	放这个自旋锁后，某个等待的“测试并设置”操作向其调用者报告锁已释放。
	
	理解自旋锁最简单的方法是把它作为一个变量看待，该变量把一个临界区或者标
	记为“我当前在运行，请稍等一会”或者标记为“我当前不在运行，可以被使用” 。
	如果 A 执行单元首先进入例程，它将持有自旋锁；当 B 执行单元试图进入同一个例
	程时，将获知自旋锁已被持有，需等到 A 执行单元释放后才能进入。
2.自旋锁相关的操作
	1．定义自旋锁         spinlock_t spin;
	2．初始化自旋锁 spin_lock_init(lock)          该宏用于动态初始化自旋锁lock
	3．获得自旋锁         spin_lock(lock)
		该宏用于获得自旋锁 lock，如果能够立即获得锁，它就马上返回，否则，它将自
	   旋在那里，直到该自旋锁的保持者释放；
		spin_trylock(lock)
		该宏尝试获得自旋锁 lock，如果能立即获得锁，它获得锁并返回真，否则立即返
		回假，实际上不再“在原地打转” ；
	4．释放自旋锁 spin_unlock(lock)
3.应用场合
	自旋锁主要针对 SMP 或单 CPU 但内核可抢占的情况，
	对于单 CPU 和内核不支持抢占的系统，自旋锁退化为空操作。
	在单 CPU 和内核可抢占的系统中，自旋锁持有期间内核的抢占将被禁止。
	由于内核可抢占的单 CPU 系统的行为实际很类似于 SMP
	系统，因此，在这样的单 CPU 系统中使用自旋锁仍十分必要。

4.注意
	1.自旋锁实际上是忙等锁，当锁不可用时，CPU 一直循环执行“测试并设置”
	该锁直到可用而取得该锁，CPU 在等待自旋锁时不做任何有用的工作，仅仅
	是等待。因此，只有在占用锁的时间极短的情况下，使用自旋锁才是合理的。
	
	2.自旋锁可能导致系统死锁。引发这个问题最常见的情况是递归使用一个自旋
	锁，即如果一个已经拥有某个自旋锁的 CPU 想第二次获得这个自旋锁，则
	该 CPU 将死锁。此外，如果进程获得自旋锁之后再阻塞，也有可能导致死
	锁的发生。copy_from_user()、copy_to_user()和 kmalloc()等函数都有可能引
	起阻塞，因此在自旋锁的占用期间不能调用这些函数。
***********************************************************/








